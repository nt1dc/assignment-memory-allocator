#define _DEFAULT_SOURCE


#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include "util.h"
#include <stdio.h>
#include <unistd.h>

static const size_t INITIAL_HEAP_SIZE = 10000;
static const size_t WANTED_SIZE = 1000;


void debug(const char *fmt, ...);

static struct block_header *get_block_from_contents(void *data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(
    struct block_header, contents));
}

void *test_heap_init() {
    debug("Creating the heap\n");
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    if (!heap) {
        err("ERROR IN CRATING HEAP");
    }
    debug("HEAP WAS SUCCESSFULLY CREATED\n\n");
    return heap;
}


void test1(struct block_header *heap) {
    debug("TEST 1\n");
    debug("MEMORY BEFORE\n");
    debug_heap(stdout, heap);
    void *block2 = _malloc(WANTED_SIZE);
    debug("MEMORY AFTER\n");
    debug_heap(stdout, heap);

    if (block2 == NULL) {
        err("MALLOC RETURN NULL o.O");
    }
    if (heap->is_free == true) {
        err("BLOCK NOT FREE");
    }
    if (heap->capacity.bytes != WANTED_SIZE) {
        err("ERROR IN CAPACITY");
    }
    debug("TEST 1 COMPLETE\n\n");
    _free(block2);
}

void test2(struct block_header *heap) {
    debug("TEST 3\n");
    void *data1 = _malloc(WANTED_SIZE);
    void *data2 = _malloc(WANTED_SIZE);
    _free(data2);
    _free(data1);
    debug_heap(stderr, heap);
    if (!((struct block_header *) (((uint8_t *) data1) - offsetof(struct block_header, contents)))->is_free) {
        err("TEST 2 ERROR");
    }
    debug("TEST 2 COMPLETE\n\n");

}

void test3(struct block_header *heap) {
    debug("TEST 3\n");
    void *data1 = _malloc(WANTED_SIZE);
    void *data2 = _malloc(1500);
    void *data3 = _malloc(3000);

    if (!data1 || !data2 || !data3) {
        err("Error TEST 3");
    }
    debug_heap(stdout, heap);
    _free(data3);
    debug_heap(stdout, heap);
    _free(data2);
    debug_heap(stdout, heap);
    debug("Test 3 COMPLETE\n\n");
    _free(data1);
}

void test4(struct block_header *heap) {
    debug("TEST 4\n");
    void *data1 = _malloc(7000);
    void *data2 = _malloc(19000);
    if (data1 == NULL || data2 == NULL) {
        err("TEST 4 ERROR");
    }
    debug_heap(stdout, heap);
    debug("Test 4 COMPLETE!\n\n");
    _free(data2);
    _free(data1);
}

void test5(struct block_header *heap) {
    debug("TEST 5\n");
    void *data1 = _malloc(10000);
    if (data1 == NULL) {
        err("TEST 5 FAILED MEMORY NOT INITIALIZED");
    }
    struct block_header *addr = heap;
    while (addr->next != NULL) addr = addr->next;
//    map_new_pages
    void *test_addr = (uint8_t *) addr + size_from_capacity(addr->capacity).bytes;
    test_addr = mmap((uint8_t * )(getpagesize() * ((size_t)
    test_addr / getpagesize() + (((size_t)
    test_addr % getpagesize()) > 0))), WANTED_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS |
                                                                     MAP_FIXED_NOREPLACE, 0, 0);
    debug(test_addr);

    void *data2 = _malloc(100000);
    debug_heap(stdout, heap);
    struct block_header *data2_block = get_block_from_contents(data2);
    if (data2_block == addr)
        err("TEST 5 ERROR");
    debug("TEST 5 COMPLETE\n\n");
}


