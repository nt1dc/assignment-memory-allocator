#ifndef _UTIL_H_
#define _UTIL_H_

#include <stddef.h>

inline size_t size_max(const size_t x,const size_t y) { return (x >= y) ? x : y; }

_Noreturn void err(const char *msg, ...);

#endif
